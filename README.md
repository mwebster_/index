# **INDEX**
**[Mark Webster's computational graphic design repositories](https://area03.bitbucket.io/)**


I'm slowly migrating from [GitHub](https://github.com/FreeArtBureau) and also setting up new repositories in line with new work. This is an on-going list of project repositories that contain further information and links to dedicated websites showcasing and/or documenting the work.

## Contents

### Projects

* [**Designing Programs**: A concise pedagogical guide to programming for visual artists](https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/)

* [**Computational Graphic Design Manual**: A complimentary resource to my Designing Programs guide](https://bitbucket.org/dpmanual/dpmanual.bitbucket.io/src/master/)

* [**Dithering**: A collection of dither algorithms](https://bitbucket.org/mwebster_/dithering/src/master/)

* [**George**: Exploring letter form](http://www.georgeandfriends.xyz/)

* [**SAM: A Machine That Draws**: Exploring computational drawing](https://bitbucket.org/samdraws/samdraws.bitbucket.io/src/master/)  

* [**Join The Dots**: A messy start in automating join the dot diagrams](https://bitbucket.org/mwebster_/join-the-dots)

* [**alphaGraph**: Exploring letter form](https://bitbucket.org/mwebster_/alphagraph)

* [**PolyArch**: Personal project working with 3D computational geometry](https://bitbucket.org/mwebster_/he_mesh/src/master/)


### Workshops

* [**Latent Type**: AI in the type deisgn process. An experimental approach.](https://latent-type.netlify.app/)

* [**Letter DeForm**: Experimenting with letter form](https://bitbucket.org/mwebster_/olivier-de-serres/src/master/)

* [**Fundamentals**: The basics concepts of programming with Processing](https://bitbucket.org/mwebster_/fundamentals/src/master/)

* [**For Loops Forever**: Iteration in graphic design](https://bitbucket.org/mwebster_/for-loops-forever/src/master/)

* [**Iterations**](https://bitbucket.org/mwebster_/iterations/src/master/)

* [...]()

### Pedagogy

* [**TOOLS & RULES**: Course project for computational graphic design](https://bitbucket.org/mwebster_/tools-rules/src/master/)

* [**P5_Intro**: A small collection of P5js sketches presented also as a website**](https://bitbucket.org/esad2020/esad2020.bitbucket.io/src/master/)

* [**_IO/OUT**: Masters course exploring computational media](https://bitbucket.org/dpinout/dpinout.bitbucket.io/src/master/)

* [**Parametric**: Exploring letter form](https://parametrictype.bitbucket.io/)

* [**Dial A Demo**: Motion exercises with interactive installation](https://bitbucket.org/mwebster_/dial-a-demo/src/master/)

* [**We Can See You**: Face detection exercise & interactive installation](https://bitbucket.org/mwebster_/we-can-see-you/src/master/)


### Tools

* [**Algorithms**](https://bitbucket.org/mwebster_/algorithms/src/master/)
  
* [**Design Patterns**](https://bitbucket.org/mwebster_/designpatterns/src/master/)
  
* [**PrintTheWeb**](https://bitbucket.org/printmyweb/printmyweb.bitbucket.io/src/master/)
* [Ancient GitHub](https://github.com/FreeArtBureau)

***

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version noVersion


## Contribute
To contribute to these projects, please contact me at the above email address.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/


